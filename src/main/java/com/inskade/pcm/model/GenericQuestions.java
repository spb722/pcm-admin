package com.inskade.pcm.model;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Objects;

//@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@MappedSuperclass
public abstract class GenericQuestions extends  BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String questionType;
    private String subType;
    private String marks;
    private Boolean hasImage= Boolean.FALSE;
    private String typeOfImage = null;
    private Long duration ;
    private String category;
    private String imageIds = null;
    private  String createdBy;
    private String classes;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        GenericQuestions that = (GenericQuestions) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
