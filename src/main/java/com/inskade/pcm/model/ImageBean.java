package com.inskade.pcm.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode(callSuper = true)
public class ImageBean extends  BaseEntity{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @CreatedDate
//    @Column(name = "created_at", nullable = false, updatable = false)
//
//    private Date createdAt;
//    @Column(name = "updated_at", nullable = false)
//    @LastModifiedDate
//    private Date updatedAt;
    private String name;
    //private String tag;
    private String s3path;
    private String sizemb;
    private String extension;
    private String folder;
    private Boolean uploaded = Boolean.FALSE;

    @OneToMany(targetEntity = TagsBean.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "cp_fk", referencedColumnName = "id")
    private List<TagsBean> tags;
    @Transient
    private String preSignedURL;

}
