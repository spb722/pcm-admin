package com.inskade.pcm.model;



import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Sachin PB
 */
@Getter

@AllArgsConstructor
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ResponseBack {
	LOGIN_SUCCESS(200, " success","the login is sucessful"),
	SUCCESS(200, " success","yep go ahead"),
	LOGIN_FAILURE(401, " failure","the login failed"),
	DEFAULT_FAILURE(400, " failure","the request failed"),
	USERNAME_UNAVILABE(400,"failure","The userName is not avilable"),
	OTP_VERIFIED(200, " success","opt verified " ),
	OTP_INCORRECT(401, " failure","otp incorrect"),
	TIME_EXPIRED(11111, " failure","the time has expired"),
	BUCKET_EXCEPTION(400, " failure","Somthing wrong while uploading "),
	NO_DETAILS_FOUND(11111, " failure","there is no detail found for this number");

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(long statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		Status = status;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	private long statusCode;
	private  String Status;
	private  String message;
	

	// Enum constructor



}


/**
@author Sachin PB 
*/