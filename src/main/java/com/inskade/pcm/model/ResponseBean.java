package com.inskade.pcm.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseBean {

    private long statusCode;
    private String Status;
    private String message;
    private String artistId;
    private String token;

    private String accountCreationLink;
    private String reason;
    private String link;
    private Object payload;

    public void setAllNulll() {
        this.payload = null;
        this.link = null;
        this.artistId = null;
        this.token = null;
        this.accountCreationLink = null;
        this.reason = null;
        this.message = null;
    }

    public void failure(String message, long code) {
        this.setAllNulll();
        this.setStatus("failure");
        this.setStatusCode(code);
        this.setMessage(message);

    }

    public void success(String message, long code, Object obj) {

        this.setAllNulll();
        this.setMessage(message);
        this.setStatus("success");
        this.setStatusCode(200);
        this.setPayload(obj);

    }

    public void success(String message, long code) {
        this.setAllNulll();
        this.setMessage(message);
        this.setStatus("success");
        this.setStatusCode(200);

    }


}
