package com.inskade.pcm.controller;

import com.inskade.pcm.model.ImageBean;
import com.inskade.pcm.model.TagsBean;
import com.inskade.pcm.service.ImageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@Slf4j
public class ImageController {

    private  final ImageService imageService ;




    @PostMapping("/saveImages")
   // @RolesAllowed("super_admin")
    public ResponseEntity<?> saveImages(@RequestBody List<ImageBean> imageBeans){
        log.info("inside saveImages");
       return imageService.saveImage(imageBeans);
    }

    @GetMapping("/getAllImages")
   //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> getAllImages(@PathParam("searchType") String searchType ,@PathParam("searchValue") String searchValue){

        return imageService.searchByType(searchType,searchValue);
    }

    @GetMapping("/getAllFolders")
    //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> getAllFolders(){

        return imageService.getAllFolders();
    }

    @GetMapping("/updateTags")
    //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> updateTags(@PathParam("tagId") Long tagId,@PathParam("tag") String tag){

        return imageService.updateTags(tagId,tag);
    }


    @PostMapping("/addTags")
    //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> addTags(@PathParam("imageId") Long imageId  , @RequestBody List<TagsBean> tagsBeans){

        return imageService.addTags(imageId, tagsBeans);
    }

    @PatchMapping("/updateImage")
  //  @PreAuthorize("hasAuthority('CUSTOMER')")
    public ResponseEntity<?> updateImage(@RequestBody Map<Object, Object> fields,
                                           @PathParam("imageId") Long imageId) {

        return imageService.updateImage(imageId,fields);

    }
    @GetMapping("/createFolder")
    //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> createFolder(@PathParam("folderName") String folderName){

        return imageService.createFolder(folderName);
    }
    @GetMapping("/getFolders")
    //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> getFolders(){

        return imageService.getFolders();
    }


}
