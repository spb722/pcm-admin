package com.inskade.pcm.controller;

import com.inskade.pcm.model.GenericQuestions;
import com.inskade.pcm.model.ImageBean;
import com.inskade.pcm.model.QuestionTypeBean;
import com.inskade.pcm.model.TagsBean;
import com.inskade.pcm.questionRepository.QuestionTypeRepository;
import com.inskade.pcm.questionmodel.*;
import com.inskade.pcm.service.QuestionService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/questions")
public class QuestionController {

    private final QuestionService questionService;
//    @PostMapping("/saveGenericQuestion")
//    @RolesAllowed("super_admin")
//    public ResponseEntity<?> saveGenericQuestion(@RequestBody GenericQuestions genericQuestions){
//        return ResponseEntity.ok(questionService.saveGenericQuestion(genericQuestions));
//    }

    @PostMapping("/countTheObjectsInTheBox")
    // @RolesAllowed("super_admin")
    public ResponseEntity<?> countTheObjectsInTheBox(@RequestBody CountTheObjectsInTheBox countTheObjectsInTheBox) {
        return questionService.countTheObjectsInTheBox(countTheObjectsInTheBox);
    }

    @PostMapping("/drawCirclesAround")
    // @RolesAllowed("super_admin")
    public ResponseEntity<?> drawCirclesAround(@RequestBody DrawCirclesAround drawCirclesAround) {
        return questionService.drawCirclesAround(drawCirclesAround);
    }

    @PostMapping("/lookAtTheImage")
    // @RolesAllowed("super_admin")
    public ResponseEntity<?> lookAtTheImage(@RequestBody LookAtTheImage lookAtTheImage) {
        return questionService.lookAtTheImage(lookAtTheImage);
    }

    @PostMapping("/matchTheFollowing")
    // @RolesAllowed("super_admin")
    public ResponseEntity<?> matchTheFollowing(@RequestBody MatchTheFollowing matchTheFollowing) {
        return questionService.matchTheFollowing(matchTheFollowing);
    }

    @PostMapping("/oddOneOut")
    // @RolesAllowed("super_admin")
    public ResponseEntity<?> oddOneOut(@RequestBody OddOneOut oddOneOut) {
        return questionService.oddOneOut(oddOneOut);
    }

    @PostMapping("/oddOneOutImage")
    // @RolesAllowed("super_admin")
    public ResponseEntity<?> countTheObjectsInTheBox(@RequestBody OddOneOutImage oddOneOutImage) {
        return questionService.oddOneOutImage(oddOneOutImage);
    }

    @PostMapping("/oddOneText")
    // @RolesAllowed("super_admin")
    public ResponseEntity<?> oddOneText(@RequestBody OddOneText oddOneText) {
        return questionService.oddOneText(oddOneText);
    }

    @PostMapping("/saveQuestionType")
    //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> saveQuestionType(@RequestBody QuestionTypeBean questionTypeBean) {

        return questionService.saveQuestionType(questionTypeBean);
    }

    @GetMapping("/getQuestionType")
    //    @RolesAllowed({"employee","admin"})
    public ResponseEntity<?> getQuestionType() {

        return questionService.getQuestionType();
    }

}
