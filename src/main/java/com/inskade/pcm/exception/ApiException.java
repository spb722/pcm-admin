package com.inskade.pcm.exception;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
@AllArgsConstructor
@Getter
public class ApiException {
private final  String message;
private final  Throwable throwable;
private final  HttpStatus httpStatus;

}

/**
@author Sachin PB 
*/