package com.inskade.pcm.exception;
/**
@author Sachin PB 
*/

import com.inskade.pcm.model.ResponseBack;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.UnexpectedRollbackException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;


@ControllerAdvice
public class ApiExceptionHandler {
	
	@ExceptionHandler(value =ApiRequestException.class)
	public ResponseEntity<Object> handleApiExceptions(ApiRequestException e){
		ApiException apiException= 	new  ApiException(e.getMessage(),e,HttpStatus.BAD_REQUEST);
		ResponseBack.DEFAULT_FAILURE.setMessage(e.getMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
		
	}
	@ExceptionHandler(value = MethodArgumentNotValidException.class)
	public ResponseEntity<?> beanValidationErrorHandling(MethodArgumentNotValidException exception){
		
		ResponseBack.DEFAULT_FAILURE.setMessage(exception.getBindingResult().getFieldError().getDefaultMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
	}
	
	@ExceptionHandler(value = javax.validation.ConstraintViolationException.class)
	public ResponseEntity<?>ConstraintViolationException(javax.validation.ConstraintViolationException exception){
		
		ResponseBack.DEFAULT_FAILURE.setMessage(exception.getMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
	}
	@ExceptionHandler(value = InvalidFormatException.class)
	public ResponseEntity<?>ConstraintViolationException(InvalidFormatException exception){
		
		ResponseBack.DEFAULT_FAILURE.setMessage(exception.getMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
	}
	@ExceptionHandler(value = MissingServletRequestParameterException.class)
	public ResponseEntity<?>MissingServletRequestParameterException(MissingServletRequestParameterException exception){
		
		ResponseBack.DEFAULT_FAILURE.setMessage(exception.getMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
	}
	@ExceptionHandler(value = java.sql.SQLIntegrityConstraintViolationException.class)
	public ResponseEntity<?>SQLIntegrityConstraintViolationException(java.sql.SQLIntegrityConstraintViolationException exception){
		
		ResponseBack.DEFAULT_FAILURE.setMessage(exception.getMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
	}
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<?>SQLIntegrityConstraintViolationException(DataIntegrityViolationException exception){
		
		ResponseBack.DEFAULT_FAILURE.setMessage("some attribute in the passed api should be unique"+exception.getCause().getMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
	}
	@ExceptionHandler(value = UnexpectedRollbackException.class)
	public ResponseEntity<?>UnexpectedRollbackException(UnexpectedRollbackException exception){
		
		ResponseBack.DEFAULT_FAILURE.setMessage("srollback excepmtion"+exception.getCause().getMessage());
		return new ResponseEntity<>(ResponseBack.DEFAULT_FAILURE,HttpStatus.BAD_REQUEST);
		
	}
	

}

