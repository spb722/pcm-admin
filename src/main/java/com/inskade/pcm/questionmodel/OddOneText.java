package com.inskade.pcm.questionmodel;

import com.inskade.pcm.model.GenericQuestions;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class OddOneText  extends  GenericQuestions{
    private String questionType;
    private String title;
    private String description;
    private String options;
    private String answer;


}
