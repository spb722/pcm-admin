package com.inskade.pcm.questionmodel;

import com.inskade.pcm.model.GenericQuestions;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)

public class LookAtTheImage extends GenericQuestions {

    private String questionType;
    private String title;
    private String description;
    private String imageId;
    private String answer;

}
