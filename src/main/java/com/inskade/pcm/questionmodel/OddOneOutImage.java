package com.inskade.pcm.questionmodel;

import com.inskade.pcm.model.GenericQuestions;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class OddOneOutImage extends GenericQuestions {
    private String questionType;
    private String title;
    private String description;
    private String images;
    private String answer;
}
