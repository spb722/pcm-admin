package com.inskade.pcm.questionmodel;

import com.inskade.pcm.model.BaseEntity;
import com.inskade.pcm.model.GenericQuestions;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;

@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
@Entity
public class CountTheObjectsInTheBox extends GenericQuestions {

    private String questionType;

    private String title;
    private String description;
    private String folderId;
    private int count;
    private int totalCount;

}
