package com.inskade.pcm.service;

import com.amazonaws.HttpMethod;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CopyObjectRequest;
import com.inskade.pcm.configuration.S3Config;
import com.inskade.pcm.exception.ApiRequestException;
import com.inskade.pcm.model.ResponseBean;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class AwsService {
    private  final AmazonS3 s3;

    private final S3Config s3Config;
    private final ResponseBean responseBean;
    private  final String BUCKET_NAME = "pcmtest3";


    public String generatePreSignedUrl(String key,String bucketName,String typeOfLoad) {
        {
            Calendar calendar;
            String filePath;
            String url = null;
            String audioPath;
            String audioUrl;
            try {
                calendar= Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.MINUTE, 10); //validity of 10 minutes

                log.info("the file path is "+key);
                if (typeOfLoad.equalsIgnoreCase("upload")){
                    url  =s3.generatePresignedUrl(bucketName, key, calendar.getTime(), HttpMethod.PUT).toString();
                }else {
                    url  =s3.generatePresignedUrl(bucketName, key, calendar.getTime(), HttpMethod.GET).toString();
                }

                return url;
            } catch (SdkClientException e) {
                log.error("error occoured in generatePreSignedUrl for the key"+key);
                throw new RuntimeException(e);

            }


        }
    }

    public void moveObject(String initalPath,String finalPath,String bucketName){
        try {
            CopyObjectRequest copyObjRequest = new CopyObjectRequest(bucketName, initalPath, bucketName, finalPath);
            s3.copyObject(copyObjRequest);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }

    }
}
