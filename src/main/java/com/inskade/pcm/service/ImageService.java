package com.inskade.pcm.service;

import com.google.gson.Gson;
import com.inskade.pcm.exception.ApiRequestException;
import com.inskade.pcm.model.FolderBean;
import com.inskade.pcm.model.ImageBean;
import com.inskade.pcm.model.ResponseBean;
import com.inskade.pcm.model.TagsBean;
import com.inskade.pcm.repository.FolderRepository;
import com.inskade.pcm.repository.ImageRepository;
import com.inskade.pcm.repository.TagRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import javax.transaction.Transactional;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
@AllArgsConstructor
public class ImageService {

    private final AwsService awsService;
    private final ImageRepository imageRepository;
    private final ResponseBean responseBean;
    private final Gson gson;
    private final TagRepository tagRepository;

    private final FolderRepository folderRepository;

    @Transactional
    public ResponseEntity<?> saveImage(List<ImageBean> imageBeans) {

        String key;
        String url;
        List<ImageBean> imageBeansResponse = new ArrayList<ImageBean>();
        FolderBean folderBean;

        try {

            for (ImageBean image : imageBeans) {
                log.info("the image bean is ", gson.toJson(image));
                key = "images/" + image.getFolder() + "/" + image.getName();
                image.setS3path(key);
                // check if image is already present in the db
                if (imageRepository.existsByS3path(key)) {
                    responseBean.failure("the path already exist with file name " +
                            "" + image.getName() + "and folder name " + image.getFolder() + "in s3 ", 400);
                    return new ResponseEntity<>(responseBean, HttpStatus.BAD_REQUEST);
                }
                url = awsService.generatePreSignedUrl(key, "image-repository-pcm-admin", "upload");
                if (null == url) {
                    responseBean.failure("error occoured while uploading the images ", 400);
                    return new ResponseEntity<>(responseBean, HttpStatus.BAD_REQUEST);
                }
                image.setPreSignedURL(url);
                imageBeansResponse.add(image);
                imageRepository.save(image);
                folderBean = folderRepository.findByFolderName(image.getFolder());
                if (ObjectUtils.isEmpty(folderBean)) {
                    throw new ApiRequestException("no folder named " + image.getName() + "present in the db");
                }
                folderBean.setSize(folderBean.getSize()+ Long.parseLong(image.getSizemb()));
                folderRepository.save(folderBean);
            }


            responseBean.success("the images has been uploaded successfully and can" +
                    " be downloaded by calling presigned url", 200, imageBeansResponse);
            return ResponseEntity.ok(responseBean);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public ResponseEntity<?> searchByType(String searchType, String searchValue) {

        List<ImageBean> imageInfo;
        String url;
        List<ImageBean> responseImageBean = new ArrayList<ImageBean>();
        switch (searchType.toLowerCase()) {

            // Case 1
            case "folder":

                // Print statement corresponding case
                log.info("the search type is by folder");
                imageInfo = imageRepository.findByFolderContainingIgnoreCase(searchValue);
                if (ObjectUtils.isEmpty(imageInfo)) {
                    responseBean.success("no images are there for folder ", 200);
                    return new ResponseEntity<>(responseBean, HttpStatus.OK);
                }

                for (ImageBean imageBean : imageInfo) {
                    url = awsService.generatePreSignedUrl(imageBean.getS3path(), "image-repository-pcm-admin", "download");
                    imageBean.setPreSignedURL(url);
                    responseImageBean.add(imageBean);
                }


                break;

            // Case 2
            case "tags":

                // Print statement corresponding case
                System.out.println("two");
                break;

            // Case 3
            case "name":

                // Print statement corresponding case
                System.out.println("three");
                break;


            default:

                // Print statement corresponding case
                log.info("no case was given  in the filter");
        }
        if (ObjectUtils.isEmpty(responseImageBean)) {
            responseBean.failure("somthing happened while getting presigned url", 400);
            return new ResponseEntity<>(responseBean, HttpStatus.NOT_FOUND);
        }
        responseBean.success("got presigned urls ", 200, responseImageBean);
        return new ResponseEntity<>(responseBean, HttpStatus.OK);
    }

    public ResponseEntity<?> updateImage(Long imageId, Map<Object, Object> fields) {
        ImageBean imageBean;
        ImageBean imageBeanSaved;

        try {

            imageBean = imageRepository.findById(imageId).orElse(null);
            if (ObjectUtils.isEmpty(imageBean)) {
                responseBean.success("no image information has been found for the id " + imageId, 404);
                return new ResponseEntity<>(responseBean, HttpStatus.NOT_FOUND);
            }
            fields.forEach((key, value) -> {
                if (key.toString().equals("name")) {
                    log.info("the filename is to be updated");
                    imageBean.setS3path(imageBean.getS3path() + value.toString());
                    String url = awsService.generatePreSignedUrl(imageBean.getS3path(), "image-repository-pcm-admin", "upload");
                    imageBean.setPreSignedURL(url);
                }
                if (key.toString().equals("folder")) {
                    log.info("the folder is to be updated");
                    if (ObjectUtils.isNotEmpty(imageBean.getName()) && imageBean.getUploaded()) {
                        String newPath = "images/" + imageBean.getFolder() + "/" + imageBean.getName();
                        awsService.moveObject(imageBean.getS3path(), newPath, "image-repository-pcm-admin");
                        imageBean.setS3path(newPath);
                    }

//                    String path = "images/" + imageBean.getFolder() + "/" + imageBean.getName();
//                    imageBean.setS3path(path);
//                    String  url = awsService.generatePreSignedUrl(imageBean.getS3path(), "image-repository-pcm-admin", "upload");
//                    imageBean.setPreSignedURL(url);
                }

                Field field = ReflectionUtils.findField(ImageBean.class, (String) key);
                field.setAccessible(true);
                ReflectionUtils.setField(field, imageBean, value);
            });
            imageBeanSaved = imageRepository.save(imageBean);
//             if (ObjectUtils.isNotEmpty(imageBeanSaved.getName())){
//                 if (imageBeanSaved.getS3path().contains(imageBeanSaved.getName())){
//                    url = awsService.generatePreSignedUrl(imageBean.getS3path(), "image-repository-pcm-admin", "upload");
//                 }else{
//                     imageBean.setS3path(imageBean.getS3path()+imageBeanSaved.getName());
//                     url = awsService.generatePreSignedUrl(imageBean.getS3path(), "image-repository-pcm-admin", "upload");
//                 }
//
//             }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        responseBean.success("the image info has been updated ", 200, imageBeanSaved);
        return ResponseEntity.ok(responseBean);
    }

    public ResponseEntity<?> getAllFolders() {
        List<String> folerList = imageRepository.findDistinctFolder();
        if (ObjectUtils.isEmpty(folerList)) {
            responseBean.success("no folers are present in db please isert one", 200);
        } else {
            responseBean.success("got all unique folderslist", 200, folerList);
        }
        return ResponseEntity.ok(responseBean);
    }

    public ResponseEntity<?> updateTags(Long tagId, String tag) {
        TagsBean tagBean = tagRepository.findById(tagId).orElse(null);
        if (ObjectUtils.isEmpty(tagBean)) {
            responseBean.failure("the tag id is not present in db pls add", 400);
            return new ResponseEntity<>(responseBean, HttpStatus.NOT_FOUND);
        }
        tagBean.setTagName(tag);
        tagRepository.save(tagBean);

        responseBean.success("the tag idhasbeen updated", 200);
        return new ResponseEntity<>(responseBean, HttpStatus.OK);

    }

    public ResponseEntity<?> addTags(Long imageId, List<TagsBean> tagsBeans) {
        ImageBean imageBean = imageRepository.findById(imageId).orElse(null);
        if (ObjectUtils.isEmpty(imageBean)) {
            responseBean.success("no images are present for the id  ", 404);
            return new ResponseEntity<>(responseBean, HttpStatus.NOT_FOUND);
        }
        imageBean.setTags(tagsBeans);
        imageRepository.save(imageBean);
        responseBean.success("tags has been added   ", 200);
        return new ResponseEntity<>(responseBean, HttpStatus.OK);
    }

    public ResponseEntity<?> createFolder(String folderName) {
        FolderBean folderBeanSaved;
        FolderBean folderBean;
        try {
            if (folderRepository.existsByFolderName(folderName)) {
                responseBean.success("the folder already exist ", 200);
                return new ResponseEntity<>(responseBean, HttpStatus.OK);
            }
            folderBean = new FolderBean();
            folderBean.setFolderName(folderName);
            folderBean.setCount(0L);
            folderBean.setSize(0L);
            folderBeanSaved = folderRepository.save(folderBean);
            responseBean.success("the folderhasbeen saved ", 200);
        } catch (Exception e) {
            throw new ApiRequestException(e.getMessage());
        }
        return new ResponseEntity<>(responseBean, HttpStatus.OK);
    }

    public ResponseEntity<?> getFolders() {
        responseBean.success("got all folders info", 200, folderRepository.findAll());

        return ResponseEntity.ok(responseBean);
    }
}
