package com.inskade.pcm.service;

import com.inskade.pcm.model.GenericQuestions;
import com.inskade.pcm.model.QuestionTypeBean;
import com.inskade.pcm.model.ResponseBean;
import com.inskade.pcm.questionRepository.*;
import com.inskade.pcm.questionmodel.*;
import com.inskade.pcm.repository.GenericQuestionRepository;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class QuestionService {

    private ResponseBean responseBean;
    private final GenericQuestionRepository genericQuestionRepository;
    private final CountTheObjectsInTheBoxRepo countTheObjectsInTheBoxRepo;
    private final DrawCirclesAroundRepo drawCirclesAroundRepo;
    private final LookAtTheImageRepo lookAtTheImageRepo;
    private final MatchTheFollowingRepo matchTheFollowingRepo;
    private final OddOneOutImageRepo oddOneOutImageRepo;
    private final OddOneOutRepo oddOneOutRepo;
    private final OddOneTextRepo oddOneTextRepo;

    private final QuestionTypeRepository typeRepository;


    public  ResponseEntity<?> saveQuestionType(QuestionTypeBean questionTypeBean)
    {
        QuestionTypeBean obj = typeRepository.save(questionTypeBean);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);
    }


    public Object saveGenericQuestion(GenericQuestions genericQuestions) {
        if (genericQuestions.getHasImage()) {
            // upload the image to the aws
        }
        genericQuestionRepository.save(genericQuestions);
        return null;
    }

    public ResponseEntity<?> countTheObjectsInTheBox(CountTheObjectsInTheBox countTheObjectsInTheBox) {
        CountTheObjectsInTheBox obj = countTheObjectsInTheBoxRepo.save(countTheObjectsInTheBox);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);

    }
    public ResponseEntity<?> drawCirclesAround(DrawCirclesAround drawCirclesAround) {
        DrawCirclesAround obj = drawCirclesAroundRepo.save(drawCirclesAround);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);

    }
    public ResponseEntity<?> lookAtTheImage(LookAtTheImage lookAtTheImage) {
        LookAtTheImage obj = lookAtTheImageRepo.save(lookAtTheImage);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);

    }
    public ResponseEntity<?> matchTheFollowing(MatchTheFollowing matchTheFollowing) {
        MatchTheFollowing obj = matchTheFollowingRepo.save(matchTheFollowing);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);

    }
    public ResponseEntity<?> oddOneOutImage(OddOneOutImage oddOneOutImage) {
        OddOneOutImage obj = oddOneOutImageRepo.save(oddOneOutImage);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);

    }
    public ResponseEntity<?> oddOneOut(OddOneOut oddOneOut) {
        OddOneOut obj = oddOneOutRepo.save(oddOneOut);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);

    }
    public ResponseEntity<?> oddOneText(OddOneText oddOneText) {
        OddOneText obj = oddOneTextRepo.save(oddOneText);
        responseBean.success("the question has been saved",200,obj);
        return ResponseEntity.ok(responseBean);

    }

    public ResponseEntity<?> getQuestionType()
    {
        List<QuestionTypeBean> obj = typeRepository.findAll();
        responseBean.success("Got All The Questions",200,obj);
        return ResponseEntity.ok(responseBean);
    }
}
