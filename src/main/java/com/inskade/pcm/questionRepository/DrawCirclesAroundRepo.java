package com.inskade.pcm.questionRepository;

import com.inskade.pcm.questionmodel.DrawCirclesAround;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DrawCirclesAroundRepo extends JpaRepository<DrawCirclesAround,Long> {
}
