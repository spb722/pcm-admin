package com.inskade.pcm.questionRepository;

import com.inskade.pcm.questionmodel.OddOneOut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OddOneOutRepo extends JpaRepository<OddOneOut,Long> {
}
