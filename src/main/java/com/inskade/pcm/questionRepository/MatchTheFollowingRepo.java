package com.inskade.pcm.questionRepository;



import com.inskade.pcm.questionmodel.MatchTheFollowing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchTheFollowingRepo extends JpaRepository<MatchTheFollowing,Long> {
}
