package com.inskade.pcm.questionRepository;

import com.inskade.pcm.model.QuestionTypeBean;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionTypeRepository extends JpaRepository<QuestionTypeBean,Long>
{

}
