package com.inskade.pcm.questionRepository;

import com.inskade.pcm.questionmodel.CountTheObjectsInTheBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountTheObjectsInTheBoxRepo extends JpaRepository<CountTheObjectsInTheBox,Long> {
}
