package com.inskade.pcm.questionRepository;

import com.inskade.pcm.questionmodel.OddOneOutImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OddOneOutImageRepo extends JpaRepository<OddOneOutImage,Long> {
}
