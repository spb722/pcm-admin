package com.inskade.pcm.questionRepository;

import com.inskade.pcm.questionmodel.OddOneText;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OddOneTextRepo extends JpaRepository<OddOneText,Long> {
}
