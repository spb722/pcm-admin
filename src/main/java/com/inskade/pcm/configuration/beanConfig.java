package com.inskade.pcm.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inskade.pcm.model.ResponseBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class beanConfig {
    @Bean
    ResponseBean responseBean(){
        ResponseBean responseBean = new ResponseBean();
        return responseBean;
    }

    @Bean
    public Gson modelmapper() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        return gson;
    }

}
