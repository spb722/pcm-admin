package com.inskade.pcm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcmAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcmAdminApplication.class, args);
	}

}
