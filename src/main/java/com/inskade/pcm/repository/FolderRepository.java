package com.inskade.pcm.repository;

import com.inskade.pcm.model.FolderBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FolderRepository extends JpaRepository<FolderBean,Long> {
    boolean existsByFolderName(String folderName);
    FolderBean findByFolderName(String name);
}
