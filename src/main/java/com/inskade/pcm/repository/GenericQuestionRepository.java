package com.inskade.pcm.repository;

import com.inskade.pcm.model.GenericQuestions;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenericQuestionRepository extends JpaRepository<GenericQuestions,Long> {
}
