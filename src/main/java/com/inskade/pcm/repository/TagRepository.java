package com.inskade.pcm.repository;

import com.inskade.pcm.model.TagsBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<TagsBean,Long> {
}
