package com.inskade.pcm.repository;


import com.inskade.pcm.model.ImageBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<ImageBean, Long> {
    public boolean existsByS3path(String s3path);
    List<ImageBean>   findByFolderContainingIgnoreCase(String folderName);



    @Query("SELECT DISTINCT a.folder FROM ImageBean a")
    List<String> findDistinctFolder();
}
